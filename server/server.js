const uuid = require("uuid/v4");
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

// const session = require("express-session"); //yet to install
// const MySQLStore = require("express-mysql-session")(session); //yet to install



const pool = mysql.createPool({ //
	host: "localhost", port: 3306,
	user: "a2", password: "Password1234",
	database: "assessment2", // to be setup
	connectionLimit: 4
});



const app = express();



app.use(bodyParser.urlencoded({extended: false })); //need to set size limit for upload
app.use(bodyParser.json());


const mkQuery = function(sql, pool) {
	return (function() {
		const defer = q.defer();
		//Collect arguments
		const args = [];
		for (var i in arguments)
			args.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}

			conn.query(sql, args, function(err, result) {
				if (err) 
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});

		return (defer.promise);
	});
}


const SQL_SEARCHBY_PRODUCT = "select * from grocery_list where name like ? ORDER BY name LIMIT 20;"; 
const SQL_SEARCHBY_BRAND = "select * from grocery_list where brand like ? ORDER BY name LIMIT 20;";
const SQL_ADD = " insert into grocery_list(upc12, brand, name) values(?, ?, ?)";
const SQL_EDIT = "UPDATE grocery_list SET brand = ? , name = ? WHERE id = ?"; // use SET only once. 

//const SQL_EDIT = "UPDATE grocery_list SET brand = ? ,SET name = ? WHERE id = ?";




const findByName = mkQuery(SQL_SEARCHBY_PRODUCT, pool);
const findByBrand = mkQuery(SQL_SEARCHBY_BRAND, pool);

//create handle error 


app.get("/api/product/:name", function (req, res) {



    const nameProduct = "%" + req.params.name + "%";
    console.log("nameProduct is %s", nameProduct);

    findByName(nameProduct)
		.then(function(results) {
            // console.log("here are results %s", results);
			if (results.length > 0) {
				const records = results;
                // console.log("the records are: %s", records )
				res.status(200);
                res.json(results);
			}
			// return (q.reject("product not found"));
		})
	
		.catch(function(err) {
			console.log("rejected");
			res.status(404);
			res.end();
		});;


});



app.get("/api/brand/:brand", function (req, res) {



    const nameBrand = "%" + req.params.brand + "%";
    console.log("nameBrand is %s", nameBrand);

    findByBrand(nameBrand)
		.then(function(results) {
            // console.log("here are results %s", results);
			if (results.length > 0) {
				const records = results;
                // console.log("the records are: %s", records )
				res.status(200);
                res.json(results);
			}
			// return (q.reject("brand not found"));
		})
	
		.catch(function(err) {
			console.log("rejected");
			res.status(404);
			res.end();
		});;


});



 app.post("/api/add", function(req, res){


	 
 	console.info(" product name is: %s", req.body.name);
    console.info(" brand name is: %s", req.body.brand);
    console.info("upc12 number is %s", req.body.upc12)


	 pool.getConnection(function(err, conn){

		 if(err){
			console.error(">>>> error: ", err);
            res.status(500);
            res.end(JSON.stringify(err));
            return;

		 }

		 conn.query(SQL_ADD //conn.query is standard regardless of app.get or app.post. 
            , [req.body.upc12, req.body.brand, req.body.name ]
            , function(err, result) {
                if (err) {
                    console.error(">>>> insert: ", err);
                    res.status(500);
                    res.end(JSON.stringify(err));
                    conn.release();
                    return;
                }
            });
			conn.release();
        console.log("added successfully");
        var successMsg = true;
        res.status(200).json(successMsg);
       

	 })

 });




app.post("/api/edit", function(req, res){


	 
 	console.info(" product name is: %s", req.body.name);
    console.info(" brand name is: %s", req.body.brand);
    console.info(" id is %s", req.body.id)
	console.info(" type of id is %s", typeof(req.body.id));

	const priId = parseInt(req.body.id);
	console.info(" type of priId is %s", typeof(priId));


	 pool.getConnection(function(err, conn){

		 if(err){
			console.error(">>>> error: ", err);
            res.status(500);
            res.end(JSON.stringify(err));
            // return;

		 }

		 conn.query(SQL_EDIT //conn.query is standard regardless of app.get or app.post. 
            , [ req.body.brand, req.body.name, priId ]
            , function(err, result) {
                if (err) {
                    console.error(">>>> insert: ", err);
                    res.status(500);
                    res.end(JSON.stringify(err));
                    conn.release();
                    // return;
                }
            });
			conn.release();
        console.log("edited successfully");
        var successMsg = true;
        res.status(200).json(successMsg);
       

	 })

 });










app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

app.use(express.static(path.join(__dirname, "../client")));


const port = 3005;

app.listen(port, function(){
    console.info("Application started on port %d", port);
	console.info("time stamp: %s", new Date());
    // console.log("\tsession key = %s", key);
})